/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"monospace:size=10"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#111111", "#F0C674" },
	[SchemeSel] = { "#F0C674", "#111111" },
	[SchemeSelHighlight] = { "#111111", "#F0C674" },
	[SchemeNormHighlight] = { "#F0C674", "#111111" },
	[SchemeOut] = { "#000000", "#00ffff" },
};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 25;
static unsigned int columns    = 8;


static unsigned int x-offset    = 300;
static unsigned int y-offset    = 100;
static unsigned int menu-width    = 1250;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 5;

static unsigned int lineheight = 0;         /* -h option; minimum height of a menu line     */
