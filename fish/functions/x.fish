function x --wraps='disown & exit' --description 'alias x=disown & exit'
  disown & exit $argv; 
end
