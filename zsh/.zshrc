autoload -Uz compinit
compinit
_comp_options+=(globdots)

PLUGINS=$HOME/.config/zsh/plugins
HISTFILE=$ZDOTDIR/.zhistory

PS1='%F{green}%n%f%F{white}@%m%f %F{blue}%1~%f> '
source $PLUGINS/zsh-autosuggestions/*.zsh
if [[ "$TERM" != "linux" ]];
then
  eval "$(starship init zsh)"
  pfetch | lolcat 
  if [[ "$(date +%w)" = "0" ]];
  then
    fortune | cowsay -f turkey | lolcat
  elif [[ "$(date +%w)" = "1" ]];
  then
    fortune | cowsay -f stimpy | lolcat
  elif [[ "$(date +%w)" = "2" ]];
  then
    fortune | cowsay -f blowfish | lolcat
  elif [[ "$(date +%w)" = "3" ]];
  then
    fortune | cowsay -f stegosaurus  | lolcat
  elif [[ "$(date +%w)" = "4" ]];
  then
    fortune | cowsay -f kitty | lolcat
  elif [[ "$(date +%w)" = "5" ]];
  then
    fortune | cowsay -f dragon | lolcat 
  elif [[ "$(date +%w)" = "6" ]];
  then
    fortune | cowsay -f turtle | lolcat
  fi
fi 
SAVEHIST=500
 source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 

 if [[ "$(tty)" = "/dev/tty1" ]];
 then
   exec startx
 fi
alias 'ls'='exa -la --icons --group-directories-first'
alias 'e'='emacsclient -c -a "emacs -nw" -nw '
