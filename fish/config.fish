# vim:set ft=sh
if status is-interactive
  # Commands to run in interactive sessions can go here
alias ls='exa -a --group-directories-first'
	if test $TERM != "linux"
       source (starship init fish --print-full-init | psub)
	   pfetch | lolcat
       	   ~/.local/share/scripts/greeter.sh
	   ~/.local/share/scripts/fortuneteller.sh
	   alias ls='exa -a --icons --group-directories-first'
	end
	alias e='emacsclient -nw -a "emacs -nw" '

    alias fish-restart="reset;exec fish"
  if test (tty) = "/dev/tty1"
     exec startx
  end
end
set fish_greeting
export "ALTERNATE_EDITOR=emacs"
export "EDITOR=emacs"
